# MULTI STEP DOCKERFILE 
#  - We need access to node to be able to install the project dependencies
#  - We also need access to nginx to serve the project

FROM node:alpine

WORKDIR '/app'

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

# after the build phase, the stuff we want to serve will be in /app/build
# this is what we copy over to the next phase

FROM nginx

EXPOSE 80

# want to copy over Something(--) FROM the 'builder' phase
COPY --from=0 /app/build /usr/share/nginx/html

# We don't need to run anything to start nginx, it will start by default
